# carteles-apps-software

Carteles diseñados para talleres de autodefensa con explicaciones de las apps y software que puedes usar. 

Tienen una URL, un QR code, descripción breve y pantallazos.

Incluyen un código mediante iconos de dispositivos y sistemas operativos.

Hazlos tuyos, copialos, replicalos, duplicalos, compartelos, extiendelos... 

larga vida al software libre.